﻿using UnityEngine.Events;
using UnityEngine.Networking;

namespace ao.PongGame
{
    public class GameEvents
    {
        // game states
        public static UnityAction OnSingleStarted;

        public static UnityAction OnHostStarted;
        public static UnityAction<string> OnClientStarted;
        public static UnityAction OnStartMenu;
        public static UnityAction OnFinishMenu;
        public static UnityAction OnNetworkMenu;

        public static UnityAction<NetworkGameType> OnStartSinglePlayerSession;
        public static UnityAction<NetworkGameType> OnStartMultiPlayerSession;

        public static UnityAction OnReplayLevel;
        public static UnityAction OnPrevLevel;
        public static UnityAction OnNextLevel;

        // player inputs
        public static UnityAction<NetworkBehaviour, float> OnPaddleMove;

        // gameplay
        public static UnityAction<string> OnGoal;

        public static UnityAction<int, int> OnScoreChanged; // bottom, upper

        // misc
        public static UnityAction<string> OnShowHostIP;
    }
}