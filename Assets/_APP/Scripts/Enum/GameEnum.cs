﻿namespace ao.PongGame
{
    public enum NetworkGameType
    {
        Single,
        Host,
        Client
    }
}