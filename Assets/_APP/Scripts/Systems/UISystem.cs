﻿using UnityEngine;

namespace ao.PongGame
{
	public class UISystem
	{
		private GameSettings _gameSettings;
		private GameMenu _gameMenu;
		private Animator _uiAnimator;

		private UIStatsSystem _uiStatsSystem;

		#region INITIALIZE AND DISPOSE

		/// <summary>
		/// setup game objects and dependencies
		/// </summary>
		/// <param name="gameSettings"></param>
		public void Initialize(GameSettings gameSettings)
		{
			_gameSettings = gameSettings;

			// create game menu
			_gameMenu = GameObject.Instantiate(_gameSettings.gameMenu);
			_uiAnimator = _gameMenu.GetComponent<Animator>();

			// create stats system
			_uiStatsSystem = new UIStatsSystem();
			_uiStatsSystem.Initialize(_gameMenu);

			// setup events
			GameEvents.OnStartMenu += StartMenu;
			GameEvents.OnSingleStarted += StartSingle;

			GameEvents.OnNetworkMenu += NetworkMenu;
			GameEvents.OnHostStarted += StartHost;
			GameEvents.OnClientStarted += StartClient;

			GameEvents.OnFinishMenu += FinishMenu;

			// setup ui callbacks
			_gameMenu.singlePlayerButton.onClick.AddListener(delegate { GameEvents.OnSingleStarted.Invoke(); });
			_gameMenu.multiplayerPlayerButton.onClick.AddListener(delegate { GameEvents.OnNetworkMenu.Invoke(); });
			_gameMenu.startHostButton.onClick.AddListener(delegate { GameEvents.OnHostStarted.Invoke(); });
			_gameMenu.startClientButton.onClick.AddListener(delegate { GameEvents.OnClientStarted(_gameMenu.hostIpInfo.text); });
			_gameMenu.replayLevelButton.onClick.AddListener(delegate { GameEvents.OnReplayLevel.Invoke(); });
			_gameMenu.prevLevelButton.onClick.AddListener(delegate { GameEvents.OnPrevLevel.Invoke(); });
			_gameMenu.nextLevelButton.onClick.AddListener(delegate { GameEvents.OnNextLevel.Invoke(); });

			GameEvents.OnShowHostIP += ShowHostIP;
		}

		/// <summary>
		/// Dispose and unsubscribe
		/// </summary>
		public void Dispose()
		{
			// events
			GameEvents.OnStartMenu -= StartMenu;
			GameEvents.OnSingleStarted -= StartSingle;
			GameEvents.OnNetworkMenu -= NetworkMenu;
			GameEvents.OnHostStarted -= StartHost;
			GameEvents.OnClientStarted -= StartClient;
			GameEvents.OnFinishMenu -= FinishMenu;
			GameEvents.OnShowHostIP -= ShowHostIP;

			// ui callbacks
			_gameMenu.singlePlayerButton.onClick.RemoveAllListeners();
			_gameMenu.multiplayerPlayerButton.onClick.RemoveAllListeners();
			_gameMenu.startHostButton.onClick.RemoveAllListeners();
			_gameMenu.startClientButton.onClick.RemoveAllListeners();
			_gameMenu.replayLevelButton.onClick.RemoveAllListeners();
			_gameMenu.nextLevelButton.onClick.RemoveAllListeners();
		}

		#endregion

		#region PUBLIC

		public void SetLeftScore(int val, int maxScore)
		{
			_uiStatsSystem.SetLeftScore(val, maxScore);
		}

		public void SetRightScore(int val, int maxScore)
		{
			_uiStatsSystem.SetRightScore(val, maxScore);
		}

		#endregion

		#region ANIMATIONS

		private void StartMenu()
		{
			_uiAnimator.SetTrigger("Start Menu");
		}

		private void StartSingle()
		{
			_uiAnimator.SetTrigger("Game");

			_uiStatsSystem.SetupStatsPanel(NetworkGameType.Single);
		}

		private void StartHost()
		{
			_uiAnimator.SetTrigger("Game");

			_uiStatsSystem.SetupStatsPanel(NetworkGameType.Host);
		}

		private void FinishMenu()
		{
			_uiAnimator.SetTrigger("Finish Menu");
		}

		private void NetworkMenu()
		{
			_uiAnimator.SetTrigger("Network Menu");
		}

		private void NetworkHostGame()
		{
			_uiAnimator.SetTrigger("Game");
		}

		private void StartClient(string text)
		{
			_uiAnimator.SetTrigger("Game");

			_uiStatsSystem.SetupStatsPanel(NetworkGameType.Client);
		}

		#endregion

		#region MISC

		private void ShowHostIP(string ip)
		{
			_gameMenu.hostIP.text = ip;
		}

		#endregion
	}
}
