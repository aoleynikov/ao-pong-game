﻿using UnityEngine.SceneManagement;

namespace ao.PongGame
{

    public class LevelsSystem
    {
        private GameSettings _gameSettings;

        private GameMenu _gameMenu;
        private int _curLevel = 0;

        private NetworkGameType _networkGameType = NetworkGameType.Client;
        private SpawnBallSystem _spawnBallSystem;
        private UISystem _uiSystem;

        private PongNetworkManager _pongNetworkManager;

        /// <summary>
        /// Setup game objects and dependencies
        /// </summary>
        public void Initialize(GameSettings gameSettings, UISystem uiSystem,
            PongNetworkManager pongNetworkManager)
        {
            _curLevel = SceneManager.GetActiveScene().buildIndex;
            _gameSettings = gameSettings;
            _uiSystem = uiSystem;
            _pongNetworkManager = pongNetworkManager;

            // create systems
            _spawnBallSystem = new SpawnBallSystem();

            // events
            GameEvents.OnStartSinglePlayerSession += StartSinglePlayerSession;
            GameEvents.OnStartMultiPlayerSession += StartMultiPlayerSession;

            GameEvents.OnReplayLevel += ReplayLevel;
            GameEvents.OnPrevLevel += RunPrevLevel;
            GameEvents.OnNextLevel += RunNextLevel;

            GameEvents.OnScoreChanged += SetScore;
        }

        /// <summary>
        /// Start single player
        /// </summary>
        private void StartSinglePlayerSession(NetworkGameType networkGameType)
        {
            _networkGameType = networkGameType;

            _spawnBallSystem.Initialize(_gameSettings, _gameSettings.levels[_curLevel]);
            _spawnBallSystem.SpawnBall();
        }

        /// <summary>
        /// Start multi player
        /// </summary>
        private void StartMultiPlayerSession(NetworkGameType networkGameType)
        {
            _networkGameType = networkGameType;

            if (_networkGameType == NetworkGameType.Host)
            {
                _spawnBallSystem.Initialize(_gameSettings, _gameSettings.levels[_curLevel]);
                _spawnBallSystem.SpawnBall();
            }
        }

        /// <summary>
        /// Replay level
        /// </summary>
        private void ReplayLevel()
        {
            Dispose();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        /// <summary>
        /// Run next level
        /// </summary>
        private void RunPrevLevel()
        {
            if (_curLevel - 1 >= 0)
            {
                Dispose();
                SceneManager.LoadScene(_gameSettings.levels[_curLevel - 1].scenePath);
            }
        }

        /// <summary>
        /// Run next level
        /// </summary>
        private void RunNextLevel()
        {
            if (_curLevel + 1 < _gameSettings.levels.Count)
            {
                Dispose();
                SceneManager.LoadScene(_gameSettings.levels[_curLevel + 1].scenePath);
            }
        }

        /// <summary>
        /// Set current score and finish level if needed
        /// </summary>
        private void SetScore(int bottomScore, int upperScore)
        {
            if (_networkGameType == NetworkGameType.Single)
            {
                _uiSystem.SetLeftScore(bottomScore + upperScore, _gameSettings.levels[_curLevel].maxSingleScore);

                if ((bottomScore + upperScore) >= _gameSettings.levels[_curLevel].maxSingleScore)
                {
                    GameEvents.OnFinishMenu.Invoke();

                    _spawnBallSystem.UnspawnBall();
                    _pongNetworkManager.RemovePlayers();
                }
            }
            else
            {
                _uiSystem.SetLeftScore(bottomScore, _gameSettings.levels[_curLevel].maxMultiScore);
                _uiSystem.SetRightScore(upperScore, _gameSettings.levels[_curLevel].maxMultiScore);

                if (bottomScore >= _gameSettings.levels[_curLevel].maxMultiScore ||
                    upperScore >= _gameSettings.levels[_curLevel].maxMultiScore)
                {
                    GameEvents.OnFinishMenu.Invoke();

                    _spawnBallSystem.UnspawnBall();
                    _pongNetworkManager.RemovePlayers();
                }
            }
        }

        /// <summary>
        /// Dispose all
        /// </summary>
        public void Dispose()
        {
            //_pongNetworkManager.Dispose();
            _uiSystem.Dispose();

            // unsubscribe events
            GameEvents.OnStartMultiPlayerSession -= StartMultiPlayerSession;
            GameEvents.OnStartSinglePlayerSession -= StartSinglePlayerSession;
            GameEvents.OnReplayLevel -= ReplayLevel;
            GameEvents.OnPrevLevel -= RunPrevLevel;
            GameEvents.OnNextLevel -= RunNextLevel;
            GameEvents.OnScoreChanged -= SetScore;
        }
    }

}

  

