﻿using UnityEngine;

namespace ao.PongGame
{
	public class GameBootstrap : MonoBehaviour
	{
		private GameSettings _gameSettings;

		// systems
		private UISystem _uiSystem;
		private LevelsSystem _levelsSystem;
		private PongNetworkManager _networkManager;

		private void Awake()
		{
			Initialize();
		}

		private void Initialize()
		{
			_gameSettings = Resources.Load("GameSettings") as GameSettings;

			if (!_gameSettings)
				return;

			// setup high-level systems and set dependencies
			_uiSystem = new UISystem();
			_uiSystem.Initialize(_gameSettings);

			_networkManager = GameObject.FindObjectOfType<PongNetworkManager>();
			_networkManager.Initialize(_gameSettings, Camera.main);

			_levelsSystem = new LevelsSystem();
			_levelsSystem.Initialize(_gameSettings, _uiSystem, _networkManager);
		}

		private void OnDestroy()
		{
			_uiSystem.Dispose();
			_levelsSystem.Dispose();
		}
	}
}