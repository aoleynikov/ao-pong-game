﻿using UnityEngine;
using UnityEngine.UI;

namespace ao.PongGame
{
	public class GameMenu : MonoBehaviour
	{
		[Tooltip("Start Single Player Mode Button")]
		public Button singlePlayerButton;
		[Tooltip("Start Multi Player Mode Button")]
		public Button multiplayerPlayerButton;

		[Tooltip("Start Host Button")]
		public Button startHostButton;
		[Tooltip("Host IP text")]
		public Text hostIP;

		[Tooltip("Start Client Button")]
		public Button startClientButton;
		[Tooltip("Host IP Input")]
		public InputField hostIpInfo;

		[Tooltip("Stats Panel")]
		public GameObject statsPanels;

		[Tooltip("Replay Level Button")]
		public Button replayLevelButton;
		[Tooltip("Prev Level Button")]
		public Button prevLevelButton;
		[Tooltip("Next Level Button")]
		public Button nextLevelButton;

		// prefabs
		[Tooltip("Left Score Prefab")]
		public ScoreMenu scoreLeft;
		[Tooltip("Right Score Prefab")]
		public ScoreMenu scoreRight;

	}
}
