﻿using UnityEngine;
using UnityEngine.Networking;

namespace ao.PongGame
{
    public class Ball : NetworkBehaviour
    {
        [SerializeField, Tooltip("Base force of ball movement")] 
        private float _force = 8;
        [SerializeField, Tooltip("Min horizontal velocity")] 
        private float _minHorizontalVel = 1;
        [SerializeField, Tooltip("Min vertical velocity")] 
        private float _minVerticalVel = 1;

        private Rigidbody2D _rigidBody;
        private float _ballForceRatio = 1;

        private void Start()
        {
            _rigidBody = GetComponent<Rigidbody2D>();
            Respawn();
        }

        public void Initialize(float ballForceRatio)
        {
            _ballForceRatio = ballForceRatio;
        }

        public void DestroyObject()
        {
            Destroy(this.gameObject);
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject.tag.Equals("Bottom"))
            {

                GameEvents.OnGoal.Invoke("Bottom");
                Respawn();
            }
            else if (col.gameObject.tag.Equals("Upper"))
            {

                GameEvents.OnGoal.Invoke("Upper");
                Respawn();
            }
        }

        private void OnCollisionEnter2D(Collision2D coll)
        {

            // additional vertical velocity if needed
            float addVerticalVel = 0;
            if (Mathf.Abs(_rigidBody.velocity.y) < _minVerticalVel)
            {
                addVerticalVel = _minVerticalVel * Mathf.Sign(_rigidBody.velocity.y);
            }

            // additional horizontal velocity if needed
            float addHorizontalVel = 0;
            if (Mathf.Abs(_rigidBody.velocity.x) < _minHorizontalVel)
            {
                addHorizontalVel = _minHorizontalVel * Mathf.Sign(_rigidBody.velocity.x);
            }

            // check collisions to player and walls
            if (coll.collider.tag.Equals("Player"))
            {
                Vector2 vel;
                vel.x = _rigidBody.velocity.x + addHorizontalVel * _force;
                vel.y = (_rigidBody.velocity.y + addVerticalVel * _force) +
                        (coll.collider.attachedRigidbody.velocity.y / 2);

            }
            else if (coll.gameObject.tag.Equals("Left"))
            {
                _rigidBody.velocity = new Vector3(_force * _ballForceRatio, _rigidBody.velocity.y + addVerticalVel, 0);
            }
            else if (coll.gameObject.tag.Equals("Right"))
            {
                _rigidBody.velocity = new Vector3(-_force * _ballForceRatio, _rigidBody.velocity.y + addVerticalVel, 0);
            }
        }

        private void Respawn()
        {
            transform.position = new Vector2(0f, 0f);
            _rigidBody.velocity = new Vector2(Mathf.Sign(Random.Range(-1f, 1f)) * _force * _ballForceRatio,
                Mathf.Sign(Random.Range(-1f, 1f)) * _force * _ballForceRatio);
        }
    }

}
