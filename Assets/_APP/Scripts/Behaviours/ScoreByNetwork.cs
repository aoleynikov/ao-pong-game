﻿using UnityEngine.Networking;

namespace ao.PongGame
{
	public class ScoreByNetwork : NetworkBehaviour
	{
		[SyncVar] 
		private int _bottomScore = 0;
		[SyncVar] 
		private int _upperScore = 0;

		private NetworkGameType _networkGameType = NetworkGameType.Client;

		#region INITIALIZE AND DISPOSE

		private void OnEnable()
		{
			_bottomScore = 0;
			_upperScore = 0;

			// events
			// game session
			GameEvents.OnStartMultiPlayerSession += StartMultiPlayerSession;
			GameEvents.OnStartSinglePlayerSession += StartSinglePlayerSession;

			// gameplay
			GameEvents.OnGoal += Goal;
		}

		private void OnDisable()
		{
			// game session
			GameEvents.OnStartMultiPlayerSession -= StartMultiPlayerSession;
			GameEvents.OnStartSinglePlayerSession -= StartSinglePlayerSession;

			// gameplay
			GameEvents.OnGoal -= Goal;
		}

		#endregion

		#region GAME SESSION

		private void StartSinglePlayerSession(NetworkGameType networkGameType)
		{
			_networkGameType = networkGameType;
		}

		private void StartMultiPlayerSession(NetworkGameType networkGameType)
		{
			_networkGameType = networkGameType;
		}

		#endregion

		#region SCORE COUNT

		private void Goal(string aimTag)
		{
			if (aimTag.Equals("Bottom"))
			{
				_bottomScore++;
				CmdUpdateScore();
			}
			else if (aimTag.Equals("Upper"))
			{
				_upperScore++;
				CmdUpdateScore();
			}
		}

		[Command]
		private void CmdUpdateScore()
		{
			RpcTellAllClientsToUpdateScore(_bottomScore, _upperScore);
		}

		[ClientRpc]
		private void RpcTellAllClientsToUpdateScore(int bottomScore, int upperScore)
		{
			GameEvents.OnScoreChanged.Invoke(bottomScore, upperScore);
		}

		#endregion
	}
}
